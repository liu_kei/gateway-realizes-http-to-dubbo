package com.example.gateway.controller;

import com.alibaba.fastjson.JSON;
import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("/get")
    public String get(){
        /** dubbo的泛化调用 */
        ReferenceConfig<GenericService> reference = new ReferenceConfig<GenericService>();
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("generic");
        reference.setApplication(applicationConfig);
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress("nacos://localhost:8848");
        reference.setRegistry(registryConfig);
        reference.setProtocol("dubbo");
        reference.setInterface("com.example.demo.service.UserService");
        reference.setVersion("1.0.0");
        reference.setGroup("test");
        reference.setTimeout(200000);
        reference.setGeneric(true);
        /** 添加缓存策略 */
//        ReferenceConfigCache cache = ReferenceConfigCache.getCache();
//        GenericService genericService = cache.get(reference);
        GenericService genericService = reference.get();

        /** 设置接口所需要的参数类型 */
        String[] parametertypes = new String[]{"java.lang.Integer"};
        String[] args = new String[]{"1"};
        Object data = genericService.$invoke("queryById", parametertypes, args);
        System.out.println(JSON.toJSONString(data));
        return JSON.toJSONString(data);
    }
}
