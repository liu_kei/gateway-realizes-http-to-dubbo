package com.example.gateway.filter;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.utils.ReferenceConfigCache;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;

@Component
public class HttpToDubboFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);
        assert route != null;
        Map<String, Object> metadata = route.getMetadata();
        ServerHttpResponse response = exchange.getResponse();
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        DataBuffer buffer = response.bufferFactory().wrap(getData(metadata));
        return response.writeWith(Mono.just(buffer));
    }

    private byte[] getData(Map<String, Object> metadata) {
        ReferenceConfig<GenericService> reference = new ReferenceConfig<>();
        Map<String, Object> referenceConfigMap = (Map) metadata.get("referenceConfig");

        ApplicationConfig applicationConfig = new ApplicationConfig();
        Map applicationConfigMap = (Map) metadata.get("applicationConfig");
        applicationConfig.setName(applicationConfigMap.get("name").toString());

        RegistryConfig registryConfig = new RegistryConfig();
        Map registryConfigMap = (Map) metadata.get("registryConfig");
        registryConfig.setAddress(registryConfigMap.get("address").toString());

        reference.setApplication(applicationConfig);
        reference.setRegistry(registryConfig);
        reference.setProtocol(referenceConfigMap.get("protocol").toString());
        reference.setInterface(referenceConfigMap.get("interface").toString());
        reference.setVersion(referenceConfigMap.get("version").toString());
        reference.setGroup(referenceConfigMap.get("group").toString());
        reference.setTimeout(Integer.parseInt((String) referenceConfigMap.get("timeout")));
        reference.setGeneric(true);
        /** 添加缓存策略 */
        ReferenceConfigCache cache = ReferenceConfigCache.getCache();
        GenericService genericService = cache.get(reference);
        //GenericService genericService = reference.get();

        /** 设置接口所需要的参数类型 */
        Map parameterTypesMap = (Map) metadata.get("parametertypes");
        Map argsMap = (Map) metadata.get("args");
        String[] parametertypes = new String[parameterTypesMap.size()];
        String[] args = new String[argsMap.size()];
        for (int i = 0; i < parameterTypesMap.size(); i++) {
            parametertypes[i]=parameterTypesMap.get(String.valueOf(i)).toString();
            args[i]=argsMap.get(String.valueOf(i)).toString();
        }

        Map data = (Map) genericService.$invoke(metadata.get("method").toString(), parametertypes, args);
        System.out.println(JSON.toJSONString(data));
        data.remove("class");

        return JSONUtil.toJsonStr(data).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
